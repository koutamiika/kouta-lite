=== Plugin Name ===
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 5.3.2
Stable tag: 1.3.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Cleans some WordPress features.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `kouta-lite.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.2 =
* Disable listing users throught REST API.

= 1.1 =
* Remove useless links from admin bar.

= 1.0 =
* Initial release.