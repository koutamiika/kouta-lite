<?php
/**
 * The plugin bootstrap file
 *
 * @link    https://koutamedia.fi
 * @since   1.0.0
 * @package Kouta_Lite
 *
 * @wordpress-plugin
 * Plugin Name:       Kouta Lite
 * Plugin URI:        https://bitbucket.org/koutamiika/kouta-lite/src
 * Description:       Cleans some WordPress features
 * Version:           1.3.0
 * Author:            Kouta Media
 * Author URI:        https://koutamedia.fi
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       kouta-lite
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Check plugin updates from BitBucket repo
 */
require 'update-checker/plugin-update-checker.php';
$update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/koutamiika/kouta-lite',
	__FILE__,
	'kouta-lite'
);

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'KOUTA_LITE_VERSION', '1.3.0' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-kouta-lite.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since 1.0.0
 */
function run_kouta_lite() {

	$plugin = new Kouta_Lite();
	$plugin->run();

}
run_kouta_lite();

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function kouta_support_dashboard_widgets() {
	wp_add_dashboard_widget(
		'kouta_support',                          // Widget slug.
		esc_html__( 'Tekninen tuki', 'kouta-lite' ), // Title.
		'kouta_support_render'                    // Display function.
	);
}
add_action( 'wp_dashboard_setup', 'kouta_support_dashboard_widgets' );

/**
 * Create the function to output the content of our Dashboard Widget.
 */
function kouta_support_render() {
	?>
	<p>Alla olevalla lomakkeella voit ottaa yhteyttä tekniseen tukeemme. Voit myös lähettää viestiä osoitteeseen <a href="mailto:support@koutamedia.fi">support@koutamedia.fi</a></p>
	<script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
	<iframe title="Feedback Form" class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://koutamedia.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&formTitle=" scrolling="no" height="650" width="100%" frameborder="0" >
	</iframe>
	<?php
}
