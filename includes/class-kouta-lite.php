<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link  https://koutamedia.fi
 * @since 1.0.0
 *
 * @package    Kouta_Lite
 * @subpackage Kouta_Lite/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Kouta_Lite
 * @subpackage Kouta_Lite/includes
 * @author     Kouta Media <miika@koutamedia.fi>
 */
class Kouta_Lite {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since  1.0.0
	 * @access protected
	 * @var    Kouta_Lite_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since  1.0.0
	 * @access protected
	 * @var    string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since  1.0.0
	 * @access protected
	 * @var    string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		if ( defined( 'KOUTA_LITE_VERSION' ) ) {
			$this->version = KOUTA_LITE_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'kouta-lite';

		$this->load_dependencies();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Kouta_Lite_Loader. Orchestrates the hooks of the plugin.
	 * - Kouta_Lite_i18n. Defines internationalization functionality.
	 * - Kouta_Lite_Admin. Defines all hooks for the admin area.
	 * - Kouta_Lite_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since  1.0.0
	 * @access private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-lite-loader.php';

		/**
		 * Remove emojis
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/features/class-kouta-lite-remove-emojis.php';

		/**
		 * Move Jquery to footer
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/features/class-kouta-lite-move-jquery.php';

		/**
		 * Limit revision
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/features/class-kouta-lite-limit-revisions.php';

		/**
		 * Disable file editing
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/features/class-kouta-lite-disable-file-edit.php';

		/**
		 * Clean junk from <head>
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/features/class-kouta-lite-clean-head.php';

		/**
		 * Clean dashboard widgets
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/features/class-kouta-lite-clean-dashboard.php';

		/**
		 * Remove update nags
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/features/class-kouta-lite-notifications.php';

		/**
		 * Customize UI
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/features/class-kouta-lite-customize-admin.php';

		/**
		 * Security
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/features/class-kouta-lite-security.php';

		$this->loader = new Kouta_Lite_Remove_Emojis();
		$this->loader = new Kouta_Lite_Move_Jquery();
		$this->loader = new Kouta_Lite_Limit_Revisions();
		$this->loader = new Kouta_Lite_Disable_File_Edit();
		$this->loader = new Kouta_Lite_Clean_Head();
		$this->loader = new Kouta_Lite_Clean_Dashboard();
		$this->loader = new Kouta_Lite_Notifications();
		$this->loader = new Kouta_Lite_Customize_Admin();
		$this->loader = new Kouta_Lite_Security();

		$this->loader = new Kouta_Lite_Loader();

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since 1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since  1.0.0
	 * @return string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since  1.0.0
	 * @return Kouta_Lite_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since  1.0.0
	 * @return string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
