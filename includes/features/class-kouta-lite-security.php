<?php
/**
 * Security features
 *
 * @package Kouta_Lite
 * @since   1.2.0
 */

class Kouta_Lite_Security {

	function __construct() {
		add_filter( 'rest_endpoints', array( $this, 'disable_user_endpoints' ), 1000 );
	}

	public static function disable_user_endpoints( $endpoints ) {

		// Don't disable API for logged in users, otherwise e.g. the author change
		// dropdown in Gutenberg will emit JavaScript errors and fail to render.
		if ( is_user_logged_in() ) {
			// Bail out without filtering anything
			return $endpoints;
		}

		// Disable listing users
		if ( isset( $endpoints['/wp/v2/users'] ) ) {
			unset( $endpoints['/wp/v2/users'] );
		}
		// Disable fetching a single user
		if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
			unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
		}
		return $endpoints;
	}

}
