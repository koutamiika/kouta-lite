<?php
/**
 * Disable file editing from admin.
 */
class Kouta_Lite_Disable_File_Edit {

	public function __construct() {
		if ( ! defined( 'DISALLOW_FILE_EDIT' ) ) {
			define( 'DISALLOW_FILE_EDIT', true );
		}
	}

}
