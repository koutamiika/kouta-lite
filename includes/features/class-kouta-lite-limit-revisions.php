<?php
/**
 * Limit revisions
 */
class Kouta_Lite_Limit_Revisions {

	public function __construct() {
		add_action( 'wp_revisions_to_keep', array( $this, 'revision_limit_number'), 10, 2 );
	}

	/**
	 * Limit revision number
	 *
	 * @param int $number current limit
	 * @param int $post_id ID of the current post
	 *
	 * @return int revision limit
	 */
	public function revision_limit_number( $number, $post_id ) {
		return 5;
	}

}
