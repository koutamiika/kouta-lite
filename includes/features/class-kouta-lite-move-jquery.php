<?php
/**
 * Move jQuery to footer
 */
class Kouta_Lite_Move_Jquery {

	public function __construct() {
		add_action( 'wp_default_scripts', array( $this, 'move_jquery') );
	}

	public function move_jquery( $wp_scripts ) {
		if ( !is_admin() ) {
			$wp_scripts->add_data('jquery',         'group', 1);
			$wp_scripts->add_data('jquery-core',    'group', 1);
			$wp_scripts->add_data('jquery-migrate', 'group', 1);
		}
	}

}
