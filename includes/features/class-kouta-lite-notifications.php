<?php
/**
 * Remove update nags
 */
class Kouta_Lite_Notifications {

	public function __construct() {
		add_action( 'admin_head', array( $this, 'remove_update_nags') );
		add_filter( 'wp_get_update_data', array( $this, 'hide_red_bubbles' ) );
	}

	/**
	 * Remove update nags
	 */
	public function remove_update_nags() {
		remove_action( 'admin_notices', 'update_nag', 3 );
	}

	/**
	 * Hide update notifications.
	 */
	function hide_red_bubbles( $update_data, $titles = '' ) {
		return array(
			'counts' => array(
			'plugins'      => 0,
			'themes'       => 0,
			'wordpress'    => 0,
			'translations' => 0,
			'total'        => 0,
			),
			'title'  => '',
		);
	}

}
