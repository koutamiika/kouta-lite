<?php
/**
 * Clean dashboard widgets
 */
class Kouta_Lite_Clean_Dashboard {

	public function __construct() {
		add_action( 'wp_dashboard_setup', array( $this, 'remove_dashboard_widgets' ) );
		remove_action( 'welcome_panel', 'wp_welcome_panel' );
	}

	/**
	 * Remove useless dashboard widgets
	 */
	public function remove_dashboard_widgets() {
		remove_meta_box('dashboard_right_now',       'dashboard', 'normal');
		remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
		remove_meta_box('dashboard_incoming_links',  'dashboard', 'normal');
		remove_meta_box('dashboard_activity',        'dashboard', 'normal');
		remove_meta_box('dashboard_plugins',         'dashboard', 'normal');
		remove_meta_box('wpseo-dashboard-overview',  'dashboard', 'normal');
		remove_meta_box('dashboard_quick_press',     'dashboard', 'side');
		remove_meta_box('dashboard_recent_drafts',   'dashboard', 'side');
		remove_meta_box('dashboard_primary',         'dashboard', 'side');
		remove_meta_box('dashboard_secondary',       'dashboard', 'side');
	}

}
