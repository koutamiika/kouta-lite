<?php
/**
 * Clean junk from head
 */
class Kouta_Lite_Clean_Head {

	public function __construct() {
		// Hide generator from HTML
		add_filter( 'the_generator', '__return_empty_string' );

		// Disable XML-RPC
		add_filter( 'xmlrpc_enabled', '__return_false' );

		// Remove X-Pingback header
		add_filter( 'wp_headers', function ( $headers ) {
			unset($headers['X-Pingback']);
			return $headers;
		});

		// Remove Pingback functionality
		add_filter( 'xmlrpc_methods', function ( $methods ) {
			unset( $methods['pingback.ping'] );
			return $methods;
		});

		// Remove junk from head
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'index_rel_link' );
		remove_action( 'wp_head', 'wlwmanifest_link' );
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
		remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
		remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
	}

}
