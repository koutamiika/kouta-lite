<?php
/**
 * Customize admin
 *
 * @since 1.1.0
 */
class Kouta_Lite_Customize_Admin {

	public function __construct() {
		add_action( 'wp_before_admin_bar_render', array( $this, 'remove_admin_bar_links' ) );
	}

	/**
	 * Remove some useless links from admin bar.
	 */
	public function remove_admin_bar_links() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu( 'wp-logo' );
		$wp_admin_bar->remove_menu( 'comments' );
	}

}
